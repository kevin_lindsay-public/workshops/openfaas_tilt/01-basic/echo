Please see the corresponding blog post [here](https://www.openfaas.com/blog/tilt/).

### Notes

- `sh`-style shell recommended. `cmd` has support, but not all commands, such as base-64 decode are easily available in `cmd`. Prefer WSL2 on Windows, where possible.
