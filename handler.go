package function

import (
	"fmt"
	"net/http"
	"os"

	handler "github.com/openfaas/templates-sdk/go-http"
)

// Handle a serverless request
func Handle(req handler.Request) (handler.Response, error) {
	var err error

	message := fmt.Sprintf("%s%s", os.Getenv("prefix"), string(req.Body))
	fmt.Println(message)

	return handler.Response{
		Body:       []byte(message),
		StatusCode: http.StatusOK,
	}, err
}
