###
# CONSTANTS
###
PROJECT_NAME = 'echo'

###
# LOAD HELPERS
###
load(
    './tilt/helmfile.Tiltfile',
    helmfile='helmfile'
)
load(
    './tilt/openfaas.Tiltfile',
    openfaas_function='openfaas_function'
)

# Define custom configuration

# The helmfile environment to pass to helmfile, defaults to "local"
config.define_string('environment')
# Whether or not we want Tilt to install openfaas for us
config.define_bool('install_openfaas')
# Whether or not to have tilt port forward to the gateway for you
config.define_bool('port_forward_gateway')
# The port opened on your local machine
config.define_string('gateway_local_port')

# Get runtime configuration
cfg = config.parse()

environment = cfg.get('environment', 'local')
install_openfaas = cfg.get('install_openfaas', False)
port_forward_gateway = cfg.get('port_forward_gateway', False)
gateway_local_port = cfg.get('gateway_local_port', '8080')

gateway_deps = []
function_deps = []

if install_openfaas:
    INSTALL_OPENFAAS = 'install openfaas'

    # https://docs.tilt.dev/api.html#api.local_resource
    # NOTE: this resource should have a readiness probe, but I'm not yet sure
    # what that would need to check
    local_resource(
        INSTALL_OPENFAAS,
        """
        kubectl apply -f https://raw.githubusercontent.com/openfaas/faas-netes/master/namespaces.yml

        helm repo add openfaas https://openfaas.github.io/faas-netes/

        helm repo update

        helm upgrade openfaas --install openfaas/openfaas \
          --namespace openfaas \
          --set generateBasicAuth=true \
          --set operator.create=true \
          --set faasnetes.imagePullPolicy="IfNotPresent"
        """
    )

    # If we're installing OpenFaaS in Tilt, the gateway should wait for it to
    # be up
    gateway_deps += [INSTALL_OPENFAAS]
    # If we're installing OpenFaaS in Tilt, the function shouldn't try to start
    # beforehand
    function_deps += [INSTALL_OPENFAAS]

if port_forward_gateway:
    GATEWAY = 'gateway'
    # https://docs.tilt.dev/api.html#api.local_resource
    local_resource(
        GATEWAY,
        cmd="""
        PASSWORD=$(kubectl -n openfaas get secret basic-auth -o jsonpath="{.data.basic-auth-password}" | base64 --decode) && \
        echo "\\nOpenFaaS password: $PASSWORD"
        """,
        serve_cmd='kubectl port-forward -n openfaas service/gateway {}:8080'.format(
            gateway_local_port
        ),
        links=[
            'http://localhost:{}'.format(gateway_local_port)
        ],
        resource_deps=gateway_deps
    )

# Create our OpenFaaS function
openfaas_function(
    # The name of the resource to create
    PROJECT_NAME,
    # The `language` of function
    'golang-http',
    # List of things this resource should wait for before starting
    resource_deps=function_deps,
    environment=environment,
)
